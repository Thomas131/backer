#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

if [ $# -eq 0 ];
then
	echo "Backuptool - Bash-Edition V0.9b";
	echo "===============================";
	echo "Creates incremental Backups using btrfs' CoW-technology";
	echo "";
	echo "Usage: ./backup /path/to/folder/to/backup";
	echo "";
	echo "Backup removal: btrfs subvolume delete ./path_to_backup/";
	echo "";
	echo "Unfinished Backup? No Problem, just execute the backup-command again until it is finished.";
	exit 1;
fi

if ! command -v btrfs &> /dev/null
then
  sudo apt install btrfs-progs;
fi

PATH_TO_BACKUP="/$1"
BACKUP_FOLDER=${1//\//_}
BACKUP_FOLDER=${BACKUP_FOLDER//./_}

if [ `echo $BACKUP_FOLDER | head -c 1` == "_" ]; then
	BACKUP_FOLDER=${BACKUP_FOLDER:1};
fi

if [ `echo -n $BACKUP_FOLDER | tail -c 1` == "_" ]; then #TODO: not working
	BACKUP_FOLDER=${BACKUP_FOLDER::-1};
fi

if [ ! -d "${BACKUP_FOLDER}" ]; then
    mkdir "${BACKUP_FOLDER}";
fi;

if [ ! -d "${BACKUP_FOLDER}/pending" ]; then
    btrfs subvolume create "${BACKUP_FOLDER}/pending";
fi;

time rsync -av --delete --no-specials --no-devices --numeric-ids --delete-excluded -x --stats -h "$PATH_TO_BACKUP" "$BACKUP_FOLDER/pending";
touch "${BACKUP_FOLDER}/pending";

btrfs subvolume snapshot -r "${BACKUP_FOLDER}/pending" "${BACKUP_FOLDER}/"$(date +%F_%R);

echo "Finished Backup";
