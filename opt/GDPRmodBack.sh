#!/usr/bin/env bash

#TODO: log everything into logfile

set -euxo pipefail

CMD="$@"
rolocked="ERROR"

echo "executing $CMD"
echo "Please make sure that there are no relevant directories besides pending and back.*"


clean_up() {
	trap - ERR EXIT SIGINT SIGTERM

	echo "$(date): executing $CMD" >> MODIFICATIONS.log

	set -o xtrace
	btrfs property set -ts . ro $rolocked
	set +o xtrace
}

for dir in back.* pending; do
	echo -e "\nentering $dir"
	cd "$dir"
	rolocked=$(btrfs property get -ts . ro | grep -o -e true -e false)

	#critical section
	trap clean_up ERR EXIT SIGINT SIGTERM
	set -o xtrace
	btrfs property set -ts . ro false
	
	eval "$CMD"
	set +o xtrace

	clean_up
	#end critical section

	cd ..
done

echo "seems to have finished successfully :)"